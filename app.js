// Express setup
const express = require('express');
const mongoose = require('mongoose'); //mongoose setup

// allows our backend application to be available for use in our frontend application
// allows us to control the apps Cross-Origin Resource Sharing Settings
const cors = require('cors'); 
const userRoutes = require('./routes/userRoutes');
// for activity course routes
const courseRoutes = require('./routes/courseRoutes');

const app = express();
// process.env.PORT || 4000 means that whatever is in the environment(Heroku) variable PORT, or 4000 the server will connect or listen to.
const port = process.env.PORT || 4000;

// Middleware
// allows all resources to access our backend application
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Main URI:
app.use('/users', userRoutes);
// for activity course routes
app.use('/courses', courseRoutes);

// mongoose connection
mongoose.connect(`mongodb+srv://elaydumpit:admin123@zuitt-batch197.pa6lt4y.mongodb.net/s37-s41?retryWrites=true&w=majority`,{

  useNewUrlParser: true,
  useUnifiedTopology: true
})

const db = mongoose.connection;
db.on('error', () => {
	console.log('Connection Error');
});
db.once('open', () => {
	console.log('Connected to MongoDB!');
});

app.listen(port, () => {
	console.log(`API is now online at port ${port}.`);
});
